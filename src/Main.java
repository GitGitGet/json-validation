import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

    //Reading file
    private static String readUsingFiles(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }
    //Constants checking symbols
    private static final char OPEN_BRACKET;

    static {
        OPEN_BRACKET = '{';
    }

    private static final char QUOTE;

    static {
        QUOTE = '"';
    }
    private static final char CLOSE_BRACKET;

    static {
        CLOSE_BRACKET = '}';
    }

    private static final char CLOSE_SQUARE_BRACKET;

    static {
        CLOSE_SQUARE_BRACKET = ']';
    }

    private static final char OPEN_SQUARE_BRACKET;

    static {
        OPEN_SQUARE_BRACKET = '[';
    }

    public static void main(String[] args) throws Exception{


        try {
            String string = readUsingFiles("/Users/Studio/Documents/ITIS/JSON1/res/data.txt");

            Check check = new Check();

            if (check.jsonChecking(string)) {
                System.out.println("JSON is valid");
                System.exit(0);
            } else {
                System.err.println("JSON is not valid");
                System.exit(-1);
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

    }
    public static class Check {

        public boolean jsonChecking(String string) {
            String result = "";
            char[] temp = string.toCharArray();

            for (int i = 0; i < temp.length; i++) {
                if (temp[i] == OPEN_BRACKET) {
                    result += "0";
                } else if (temp[i] == QUOTE) {
                    result += "1";
                } else if (temp[i] == CLOSE_BRACKET) {
                    result += "0";
                } else if (temp[i] == CLOSE_SQUARE_BRACKET) {
                    result += "2";
                } else if (temp[i] == OPEN_SQUARE_BRACKET) {
                    result += "2";
                }
            }
            //JSON is valid, if even number of elements, both total number and small logical sections of code
            return bracketChecking(temp) && squareBracketChecking(temp) && quotesChecking(result);
        }

        public boolean quotesChecking(String result) {
            boolean flag = false;
            List<String> list = new ArrayList<>();
            char[] quotesArray = result.toCharArray();
            for (int i = 0; i < quotesArray.length; i++) {
                String element = "";
                if (quotesArray[i] == '1') {
                    element += quotesArray[i];
                }
                list.add(element);
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).length() % 2 == 0) {
                    flag = true;
                } else {
                    flag = false;
                }
            }
            return flag;
        }
        public boolean squareBracketChecking(char[] temp) {
            boolean flag = false;
            int squareBracketCount = 0;
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] == '2') {
                    squareBracketCount ++;
                }
            }
            if (squareBracketCount == 0 || squareBracketCount % 2 == 0) {
                flag = true;
            } else {
                flag = false;
            }
            return flag;
        }
        public boolean bracketChecking(char[] temp) {
            boolean flag = false;
            int bracketCount = 0;
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] == '0') {
                    bracketCount ++;
                }
            }
            if (bracketCount == 0 || bracketCount % 2 == 0) {
                flag = true;
            } else {
                flag = false;
            }
            return flag;
        }
    }
}
